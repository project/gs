<?php

/**
 * @file
 * Primary module hooks for GreenSock module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function gs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.gs':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('GreenSock integration with Drupal.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_page_attachments().
 */
function gs_page_attachments(array &$attachments) {
  $settings = \Drupal::config('gs.settings');
  $attachments['#cache']['tags'][] = 'config:gs.settings';

  if (!\Drupal::service('gs.config.helper')->shouldAttachLibrary()) {
    return;
  }

  $attachments['#attached']['library'][] = 'gs/gsap';

  $plugins = $settings->get('gs_plugins');
  foreach ($plugins as $plugin) {
    $attachments['#attached']['library'][] = 'gs/' . $plugin;
  }
}

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function gs_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $default_theme = \Drupal::service('theme_handler')->getDefault();
  if ($form['var']['#value'] != "theme_{$default_theme}_settings") {
    return;
  }

  $settings = \Drupal::config('gs.settings');

  if (!$settings->get('enabled') || empty(\Drupal::service('gs.config.helper')->getEnabledSubmodules())) {
    return;
  }

  $form['gs'] = [
    '#type' => 'details',
    '#title' => t('Animations'),
    '#open' => FALSE,
    '#weight' => -20,
  ];
  $form['gs']['gs_tabs'] = [
    '#type' => 'vertical_tabs',
  ];
}
