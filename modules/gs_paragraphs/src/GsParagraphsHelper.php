<?php

namespace Drupal\gs_paragraphs;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\gs\GsConfigHelper;

/**
 * Class GsParagraphsHelper service.
 *
 * @package Drupal\gs_paragraphs
 */
class GsParagraphsHelper {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The admin toolbar tools configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The GsConfigHelper service.
   *
   * @var \Drupal\gs\GsConfigHelper
   */
  protected $gsapConfigHelper;

  /**
   * Constructs a GsParagraphsHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory mservice.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\gs\GsConfigHelper $gsap_config_helper
   *   The GsConfigHelper service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    ConfigFactoryInterface $config_factory,
    EntityFieldManagerInterface $entity_field_manager,
    ModuleHandlerInterface $module_handler,
    GsConfigHelper $gsap_config_helper,
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->config = $config_factory->getEditable('gs_paragraphs.settings');
    $this->entityFieldManager = $entity_field_manager;
    $this->moduleHandler = $module_handler;
    $this->gsapConfigHelper = $gsap_config_helper;
  }

  /**
   * Get Properties keys.
   *
   * @return array
   *   Array of properties.
   */
  private function getProperties() {
    return [
      'before' => [
        'title' => $this->t('Values Before Animations'),
        'x' => [
          'title' => $this->t('X value'),
          'description' => $this->t('The value X where the block should be positioned from start.'),
          'default_value' => 0,
        ],
        'y' => [
          'title' => $this->t('Y value'),
          'description' => $this->t('The value Y where the block should be positioned from start.'),
          'default_value' => 0,
        ],
        'scale' => [
          'title' => $this->t('Scale'),
          'description' => $this->t('Scale of the element before the animation starts.'),
          'default_value' => 1,
        ],
        'opacity' => [
          'title' => $this->t('Opacity'),
          'description' => $this->t('Opacity before the animation starts.'),
          'default_value' => 1,
        ],
        'rotation' => [
          'title' => $this->t('Rotation'),
          'description' => $this->t('Rotation before the animation starts.'),
          'default_value' => 0,
        ],
      ],
      'after' => [
        'title' => $this->t('Values After Animations'),
        'x' => [
          'title' => $this->t('X value'),
          'description' => $this->t('The value X where the block should be positioned at the end.'),
          'default_value' => 0,
        ],
        'y' => [
          'title' => $this->t('Y value'),
          'description' => $this->t('The value Y where the block should be positioned at the end.'),
          'default_value' => 0,
        ],
        'scale' => [
          'title' => $this->t('Scale'),
          'description' => $this->t('Scale of the element after the animation ends.'),
          'default_value' => 1,
        ],
        'opacity' => [
          'title' => $this->t('Opacity'),
          'description' => $this->t('Opacity after the animation ends.'),
          'default_value' => 1,
        ],
        'rotation' => [
          'title' => $this->t('Rotation'),
          'description' => $this->t('Rotation after the animation ends.'),
          'default_value' => 0,
        ],
      ],
      'general' => [
        'title' => $this->t('Animation configurations'),
        'delay' => [
          'title' => $this->t('Delay'),
          'description' => $this->t('Amount of delay before the animation should begin (in seconds).'),
          'default_value' => 0,
        ],
        'duration' => [
          'title' => $this->t('Duration'),
          'description' => $this->t('The duration of the animation (in seconds).'),
          'default_value' => 1,
        ],
      ],
    ];
  }

  /**
   * Create the form array for paragraphs.
   *
   * @return array
   *   Array with the form information.
   */
  public function createForm() {
    $form = [
      '#type' => 'details',
      '#title' => $this->t('Paragraphs'),
      '#group' => 'gs_tabs',
      '#tree' => TRUE,
    ];

    $this->createFormOptionsPerBundle($form);

    return $form;
  }

  /**
   * Create the form options for each paragraph bundle.
   *
   * @param array $form
   *   The current form.
   */
  private function createFormOptionsPerBundle(array &$form) {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('paragraph');
    $paragraphs_values = $this->config->get('paragraphs') ?? [];

    foreach ($bundles as $bundle => $bundle_info) {
      if ($bundle == 'from_library') {
        continue;
      }
      $form[$bundle] = [
        '#type' => 'details',
        '#open' => FALSE,
        '#title' => $bundle_info['label'],
      ];

      $form[$bundle]['enable'] = [
        '#title' => $this->t('Enable'),
        '#type' => 'checkbox',
        '#default_value' => empty($paragraphs_values) ? 0 : $paragraphs_values[$bundle]['enable'] ?? 0,
      ];

      $form[$bundle]['configs'] = [
        '#title' => $this->t('Configs'),
        '#type' => 'details',
        '#open' => TRUE,
        '#attributes' => ['class' => ['gs_paragraph']],
        '#states' => [
          'visible' => [
            ':input[name="' . "paragraphs[{$bundle}][enable]" . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      if (!empty($this->gsapConfigHelper->getEnabledPlugins())) {
        $form[$bundle]['configs']['extra_options'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Extra options'),
        ];
      }
      $this->createGsapAnimationOptions($form[$bundle]['configs'], $bundle, $paragraphs_values);
      $this->createBundleFieldsOptions(
        $form[$bundle],
        $bundle,
        $bundles,
        $paragraphs_values,
        "paragraphs[{$bundle}]",
        "paragraphs[{$bundle}][fields]",
        $paragraphs_values[$bundle] ?? [],
        $paragraphs_values[$bundle]['fields'] ?? [],
      );
    }
  }

  /**
   * Create the form options for GSAP.
   *
   * @param array $form
   *   The current form.
   * @param string $bundle
   *   The paragraph bundle.
   * @param array $paragraphs_values
   *   The config values of paragraphs.
   * @param bool $field
   *   If it is a paragraph field or not.
   * @param string $field_value
   *   The value fo the current field.
   */
  private function createGsapAnimationOptions(array &$form, $bundle, array $paragraphs_values, $field = FALSE, $field_value = []) {
    $plugins = $this->gsapConfigHelper->getEnabledPlugins();
    $data = $this->getProperties();

    foreach ($data as $type => $properties) {
      $form[$type] = [
        '#type' => 'fieldset',
        '#title' => $properties['title'],
      ];
      unset($properties['title']);
      foreach ($properties as $property => $property_values) {
        $value = $property_values['default_value'];
        if (!empty($paragraphs_values)) {
          $value = $paragraphs_values[$bundle]['configs'][$type][$property] ?? 0;
          if ($field) {
            $value = $field_value[$bundle]['configs'][$type][$property] ?? 0;
          }
        }
        $form[$type][$property] = [
          '#type' => 'number',
          '#step' => 0.01,
          '#title' => $property_values['title'],
          '#description' => $property_values['description'],
          '#default_value' => $value,
        ];
      }
    }
    if (!empty($plugins)) {
      $value = $paragraphs_values[$bundle]['configs']['extra_options']['plugins'] ?? ['scrolltrigger'];
      if ($field) {
        $value = $field_value[$bundle]['configs']['extra_options']['plugins'] ?? ['scrolltrigger'];
      }
      $form['extra_options']['plugins'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Plugins'),
        '#description' => $this->t('Select the plugins to include as libraries.'),
        '#default_value' => $value,
        '#options' => $plugins,
      ];
    }
  }

  /**
   * Get the informations about the paragraph fields.
   *
   * @param string $bundle
   *   The paragraph bundle.
   *
   * @return array
   *   Array with informations about the paragraph fields.
   */
  public function getParagraphFields($bundle) {
    $entityFieldManager = $this->entityFieldManager;
    $fields = $entityFieldManager->getFieldDefinitions('paragraph', $bundle);
    $paragraph_fields = array_filter($fields, function ($field) {
      return (method_exists($field, 'get') && $field->get('field_type') == 'entity_reference_revisions' &&  $field->get('entity_type') == 'paragraph');
    });
    $fields_info = [];
    foreach ($paragraph_fields as $field) {
      $target_bundles = $field->get('settings')['handler_settings']['target_bundles'];
      unset($target_bundles['from_library']);
      $fields_info[] = [
        'name' => $field->get('field_name'),
        'bundle' => array_values($target_bundles)[0],
        'multivalue' => $field->getFieldStorageDefinition()->get('cardinality') != 1,
      ];
    }
    return $fields_info;
  }

  /**
   * Get form elements for paragraph fields.
   *
   * @param array $form
   *   The current form element.
   * @param string $bundle
   *   The paragraph bundle.
   * @param string $bundles
   *   The paragraph bundle informations.
   * @param array $paragraphs
   *   The saved configuration values.
   * @param string $configure_fields
   *   The configure_fields path to use in recursion.
   * @param string $field_enable
   *   The field_enable path to use in recursion.
   * @param array $configure_fields_value
   *   The configure_fields value to use in recursion.
   * @param array $field_enable_value
   *   The field_enable value to use in recursion.
   */
  public function createBundleFieldsOptions(array &$form, $bundle, $bundles, array $paragraphs, $configure_fields, $field_enable, array $configure_fields_value, array $field_enable_value) {
    $paragraph_bundles = $this->entityTypeBundleInfo->getBundleInfo('paragraph');
    $paragraph_fields = $this->getParagraphFields($bundle);
    if (empty($paragraph_fields)) {
      return;
    }
    $form['configure_fields'] = [
      '#title' => $this->t('Enable fields configuration'),
      '#type' => 'checkbox',
      '#default_value' => $configure_fields_value['configure_fields'] ?? 0,
    ];
    $form['fields'] = [
      '#title' => $this->t('@bundle fields', ['@bundle' => $paragraph_bundles[$bundle]['label']]),
      '#type' => 'details',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="' . "{$configure_fields}[configure_fields]" . '"]' => ['checked' => TRUE],
        ],
      ],
    ];
    foreach ($paragraph_fields as $field) {
      $field_bundle = $field['bundle'];
      $form['fields'][$field_bundle] = [
        '#title' => $this->t('Configs for @field_name (@field_bundle)', [
          '@field_name' => $field['name'],
          '@field_bundle' => $paragraph_bundles[$field_bundle]['label'],
        ]),
        '#type' => 'fieldset',
        '#open' => TRUE,
      ];
      $form['fields'][$field_bundle]['enable'] = [
        '#title' => $this->t('Enable'),
        '#type' => 'checkbox',
        '#default_value' => $field_enable_value[$field_bundle]['enable'] ?? 0,
      ];
      $form['fields'][$field_bundle]['inherit'] = [
        '#title' => $this->t('Inherit animations for this "@field_bundle" field', ['@field_bundle' => $paragraph_bundles[$field_bundle]['label']]),
        '#type' => 'checkbox',
        '#default_value' => $field_enable_value[$field_bundle]['inherit'] ?? 1,
        '#states' => [
          'visible' => [
            ':input[name="' . "{$field_enable}[{$field_bundle}][enable]" . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['fields'][$field_bundle]['configs'] = [
        '#title' => $this->t('Configs'),
        '#type' => 'details',
        '#open' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="' . "{$field_enable}[{$field_bundle}][enable]" . '"]' => ['checked' => TRUE],
            ':input[name="' . "{$field_enable}[{$field_bundle}][inherit]" . '"]' => ['checked' => FALSE],
          ],
        ],
      ];
      if ($field['multivalue']) {
        $form['fields'][$field_bundle]['configs']['extra_options'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('General Options'),
        ];
        $form['fields'][$field_bundle]['configs']['extra_options']['interval'] = [
          '#type' => 'number',
          '#step' => 0.01,
          '#title' => $this->t('Interval between each item'),
          '#description' => $this->t('The delay interval between each item'),
          '#default_value' => $field_enable_value[$field_bundle]['configs']['extra_options']['interval'] ?? 0,
        ];
      }
      $this->createGsapAnimationOptions($form['fields'][$field_bundle]['configs'], $field_bundle, $paragraphs, TRUE, $field_enable_value);
      $this->createBundleFieldsOptions(
        $form['fields'][$field_bundle],
        $field_bundle,
        $bundles,
        $paragraphs,
        $configure_fields . "[fields][$field_bundle]",
        $field_enable . "[$field_bundle][fields]",
        $configure_fields_value['fields'][$field_bundle] ?? [],
        $field_enable_value[$field_bundle]['fields'] ?? [],
      );
    }
  }

  /**
   * Custom submit fuction.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function formSubmit(array $form, FormStateInterface $form_state) {
    $bundles = array_keys($this->entityTypeBundleInfo->getBundleInfo('paragraph'));

    $paragraph_values = $form_state->getValues()['paragraphs'];

    $paragraphs = [];
    foreach ($bundles as $bundle) {
      if ($bundle == 'from_library') {
        continue;
      }
      $paragraphs[$bundle] = $paragraph_values[$bundle];
    }
    $this->config
      ->clear('paragraphs')
      ->set('paragraphs', $paragraphs)
      ->save();
  }

}
