/**
 * @file
 * Perform anything related with the components.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.GsParagraphs = Drupal.GsParagraphs || {
    animateFrom(elem, data, $interval_delay) {
      gsap.fromTo(elem,
        {
          x: parseFloat(data['before']['x']),
          y: parseFloat(data['before']['y']),
          autoAlpha: data['before']['opacity'],
          rotation: data['before']['rotation'],
          scale: parseFloat(data['before']['scale']),
        },
        {
          x: parseFloat(data['after']['x']),
          y: parseFloat(data['after']['y']),
          autoAlpha: data['after']['opacity'],
          rotation: data['after']['rotation'],
          scale: parseFloat(data['after']['scale']),
          delay: parseFloat(data['general']['delay']) + $interval_delay,
          duration: parseFloat(data['general']['duration']),
        },
      );
    },
    addTrigger($elem, $data, $interval_delay = 0) {
      if (!$data['extra_options']['plugins'])  {
        Drupal.GsParagraphs.animateFrom($elem, $data, $interval_delay);
      }

      for ($plugin in $data['extra_options']['plugins']) {
        if ($data['extra_options']['plugins'][$plugin] == 0) {
          Drupal.GsParagraphs.animateFrom($elem, $data, $interval_delay);
          continue
        }
        // Implement the Strategy Pattern instead of using switch.
        switch ($plugin) {
          case 'scrolltrigger':
            gsap.registerPlugin(ScrollTrigger);
            ScrollTrigger.create({
              trigger: $elem,
              onEnter: function() { Drupal.GsParagraphs.animateFrom($elem, $data, $interval_delay) },
            });
            break;
        }
      }
    },
    getParentParagraphs(main = "main") {
      var $paragraphs = $(main).find('.paragraph:not(.paragraph--type--from-library)');
      var $parentParagraphs = [];
      for (var i = 0; i < $paragraphs.length; i++) {
        var hasParent = false;
        for (var j = 0; j < $paragraphs.length; j++) {
          if ($($paragraphs[i]).parents().index($paragraphs[j]) >= 0) {
            hasParent = true;
            j = $paragraphs.length
          }
        }
        if (!hasParent) {
          $parentParagraphs.push($paragraphs[i])
        }
      }
      return $parentParagraphs;
    },
    getParentParagraphByType($parentParagraphs, $type) {
      var $paragraphs = []
      $parentParagraphs.forEach(element => {
        if (element.className.split(" ")[1].includes($type.replaceAll('_', '-'))) {
          $paragraphs.push(element)
        }
      });
      return $paragraphs;
    },
    addTriggetToFields($value, $field_elem, $paragraphs_data) {
      if (!$value['enable']) {
        return;
      }
      for (var index in $field_elem.toArray()) {
        if ($value['inherit']) {
          this.addTrigger($field_elem[index], $paragraphs_data['configs']);
          continue;
        }
        var $interval = $value['configs']['extra_options']['interval'] || 0;
        this.addTrigger($field_elem[index], $value['configs'],(index * $interval))
      }
    },
    configureParagraphFields($paragraphs_data, $parentParagraphs, $bundle, $value)  {
      if (!$value['configure_fields']) {
        return;
      }
      for (var $field_bundle in $value['fields']) {
        this.getParentParagraphByType($parentParagraphs, $bundle).forEach(elem => {
          var $field_elem = $(elem).find('.paragraph--type--'.concat($field_bundle.replaceAll('_', '-')))
          if ($field_elem.length == 0) {
            return;
          }
          this.addTriggetToFields($value['fields'][$field_bundle], $field_elem, $paragraphs_data[$field_bundle]);
          $parentParagraphsInsideField = this.getParentParagraphs(elem);
          this.configureParagraphFields($paragraphs_data, $parentParagraphsInsideField, $field_bundle, $value['fields'][$field_bundle]);
        });
      }
    },
    init() {
      var $paragraphs_data = drupalSettings.gs_paragraphs.paragraphs;

      $parentParagraphs = this.getParentParagraphs();
      for (var $bundle in $paragraphs_data) {
        if ($paragraphs_data[$bundle]['enable']) {
          this.getParentParagraphByType($parentParagraphs,  $bundle).forEach(elem => {
            this.addTrigger(elem, $paragraphs_data[$bundle]['configs']);
          });
        }
        this.configureParagraphFields($paragraphs_data, $parentParagraphs, $bundle, $paragraphs_data[$bundle]);
      }
    }
  };
  Drupal.behaviors.gsapParagraphAnimation = {
    attach(context) {
      Drupal.GsParagraphs.init();
    },
  };
})(jQuery, Drupal, drupalSettings);
