<?php

namespace Drupal\gs\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gs\GsConfigHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Admin settings for GreenSock.
 *
 * @package Drupal\gs\Form
 */
class GsSettingsForm extends ConfigFormBase {

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The GsConfigHelper service.
   *
   * @var \Drupal\gs\GsConfigHelper
   */
  protected $gsConfigHelper;

  /**
   * AdminToolbarSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\gs\GsConfigHelper $gsap_config_helper
   *   The GsConfigHelper service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeBundleInfoInterface $entity_type_bundle_info, GsConfigHelper $gsap_config_helper) {
    parent::__construct($config_factory);
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->gsConfigHelper = $gsap_config_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.bundle.info'),
      $container->get('gs.config.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gs_admin';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gs.settings');
    $form = parent::buildForm($form, $form_state);

    $form['enabled'] = [
      '#title' => $this->t('Enable GSAP'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('enabled') ?? FALSE,
    ];
    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Additional Options'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['advanced']['gs_all_pages'] = [
      '#title' => $this->t('Enable GSAP on every page'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('gs_all_pages') ?? FALSE,
    ];
    $form['advanced']['gs_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Path list'),
      '#default_value' => implode(PHP_EOL, $config->get('gs_pages') ?? []),
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. Leave blank for reloading all pages. Leave the list empty to reload on all pages. This field does not support url aliases.", [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
      ]),
      '#states' => [
        'disabled' => [
          ['input[name="gs_all_pages"]' => ['checked' => TRUE]],
        ],
      ],
    ];
    $form['advanced']['gs_content_types'] = [
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('Select which content types to enable GSAP.'),
      '#type' => 'checkboxes',
      '#default_value' => $config->get('gs_content_types') ?? [],
      '#options' => $this->getContentTypes(),
      '#states' => [
        'disabled' => [
          ['input[name="gs_all_pages"]' => ['checked' => TRUE]],
        ],
      ],
    ];
    $form['advanced']['plugins'] = [
      '#type' => 'details',
      '#title' => $this->t('Plugins'),
      '#open' => TRUE,
    ];
    $form['advanced']['plugins']['gs_plugins'] = [
      '#title' => $this->t('Available CDN Libraries'),
      '#description' => $this->t('Select the plugins to include as libraries.'),
      '#type' => 'checkboxes',
      '#default_value' => $config->get('gs_plugins') ?? [],
      '#options' => $this->gsConfigHelper->getAvailablePlugins(),
    ];
    $this->disablePlugins($form);

    return $form;
  }

  /**
   * Return the formatted list of content types.
   *
   * @return array
   *   The formatted list of content types.
   */
  private function getContentTypes() {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('node');
    $content_types = [];
    foreach ($bundles as $bundle => $bundle_info) {
      $content_types[$bundle] = $bundle_info['label'];
    }

    return $content_types;
  }

  /**
   * Function to disable the plugins we aren't supporting yet.
   *
   * @param array $form
   *   The current form.
   */
  public function disablePlugins(array &$form) {
    $disabled_plugins = [
      'flip',
      'observer',
      'scrollto',
      'draggable',
      'easel',
      'motionpath',
      'pixi',
      'text',
    ];
    foreach ($disabled_plugins as $plugin) {
      $current_title = (string) $form['advanced']['plugins']['gs_plugins']['#options'][$plugin];
      $form['advanced']['plugins']['gs_plugins'][$plugin] = [
        '#disabled' => TRUE,
        '#title' => $this->t('@plugin - (Future Implementation)', ['@plugin' => $current_title]),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gs_plugins = array_values(array_filter($form_state->getValue('gs_plugins')));
    $gs_content_types = array_values(array_filter($form_state->getValue('gs_content_types')));

    $routes_list = preg_split("/(\r\n|\n|\r)/", $form_state->getValue('gs_pages'));
    $routes_list = array_map('trim', $routes_list);

    $this->config('gs.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('gs_all_pages', $form_state->getValue('gs_all_pages'))
      ->set('gs_pages', $routes_list)
      ->set('gs_plugins', $gs_plugins)
      ->set('gs_content_types', $gs_content_types)
      ->save();
    drupal_static_reset('getEnabledPlugins');
    parent::submitForm($form, $form_state);
  }

}
