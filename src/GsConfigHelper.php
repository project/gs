<?php

namespace Drupal\gs;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Class GsConfigHelper service.
 *
 * @package Drupal\gs
 */
class GsConfigHelper {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The admin toolbar tools configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The current path service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private $currentPath;

  /**
   * The alias manager service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  private $aliasManager;

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   *   The route match.
   */
  protected $routeMatch;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs a GsConfigHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory mservice.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match interface.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    CurrentPathStack $current_path,
    AliasManagerInterface $alias_manager,
    RouteMatchInterface $route_match,
    AdminContext $admin_context,
    ModuleHandlerInterface $module_handler,
    ModuleExtensionList $extension_list_module,
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('gs.settings');
    $this->currentPath = $current_path;
    $this->aliasManager = $alias_manager;
    $this->routeMatch = $route_match;
    $this->adminContext = $admin_context;
    $this->moduleHandler = $module_handler;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * Return if GSAP is enable or not.
   *
   * @return bool
   *   If GSAP is enable.
   */
  public function isGsapEnabled() {
    return $this->config->get('enabled');
  }

  /**
   * Get gs_all_pages.
   *
   * @return array
   *   Get gs_all_pages.
   */
  public function getGsOnAllPages() {
    return $this->config->get('gs_all_pages');
  }

  /**
   * Get gs_pages.
   *
   * @return array
   *   Get gs_pages.
   */
  public function getGsPages() {
    return $this->config->get('gs_pages');
  }

  /**
   * Get gs_content_types.
   *
   * @return array
   *   Get gs_content_types.
   */
  public function getGsContentTypes() {
    return $this->config->get('gs_content_types');
  }

  /**
   * Get the enabled GSAP plugins.
   *
   * @return array
   *   Array of enabled plugins.
   */
  public function getEnabledPlugins() {
    $plugins = &drupal_static(__FUNCTION__);
    if (isset($plugins)) {
      return $plugins;
    }
    if (!$this->isGsapEnabled()) {
      return [];
    }

    $plugins = $this->getPluginNames($this->config->get('gs_plugins'));
    return $plugins;
  }

  /**
   * Get the list of avaialble GSAP plugins.
   *
   * @return array
   *   Array of enabled plugins.
   */
  public function getAvailablePlugins() {
    return [
      'flip' => $this->t('Flip'),
      'scrolltrigger' => $this->t('ScrollTrigger'),
      'observer' => $this->t('Observer'),
      'scrollto' => $this->t('ScrollTo'),
      'draggable' => $this->t('Draggable'),
      'easel' => $this->t('Easel'),
      'motionpath' => $this->t('MotionPath'),
      'pixi' => $this->t('Pixi'),
      'text' => $this->t('Text'),
    ];
  }

  /**
   * Get an array with plugin machine_name and name.
   *
   * @param array $plugins
   *   Machine name of plugins.
   *
   * @return array
   *   An array with plugin machine_name and name
   */
  private function getPluginNames(array $plugins) {
    $all_plugins = $this->getAvailablePlugins();
    $return = [];

    foreach ($plugins as $plugin) {
      $return[$plugin] = $all_plugins[$plugin];
    }
    return $return;
  }

  /**
   * Get if the libraries should be attach to the page or not.
   *
   * @return bool
   *   If the libraries should be attach to the page or not.
   */
  public function shouldAttachLibrary() {
    if (!$this->isGsapEnabled()) {
      return FALSE;
    }

    if (!$this->getGsOnAllPages()) {
      $gs_pages = $this->getGsPages();
      $current_path = $this->currentPath->getPath();
      $alias = $this->aliasManager->getAliasByPath($current_path);

      $gs_content_types = $this->getGsContentTypes();
      $content_type = $this->routeMatch->getParameter('node')?->bundle();

      if ((empty($gs_pages) || !in_array($alias, $gs_pages)) && (is_null($gs_content_types) || !in_array($content_type, $gs_content_types))) {
        return FALSE;
      }
    }
    if ($this->adminContext->isAdminRoute($this->routeMatch->getRouteObject())) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get array of enabled submodules.
   *
   * @return array
   *   Array of enabled submodules.
   */
  public function getEnabledSubmodules() {
    $submodules = array_keys($this->moduleExtensionList->get('gs')->required_by);
    $enabled_submodules = [];

    foreach ($submodules as $submodule) {
      if ($this->moduleHandler->moduleExists($submodule)) {
        $enabled_submodules[] = $submodule;
      }
    }

    return $enabled_submodules;
  }

}
