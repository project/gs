## INTRODUCTION

Module to integrate the GreenSock GSAP libraries and plugins with Drupal.
With this module you can configure the GSAP animations from the CMS, without writing code to it, the module attaches the GSAP library into the pages you want and let yout configure the details for the animations.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Content Authoring > GreenSock Settings and configure it
      Enable GSAP -> Enable the module
      Enable GSAP on every page -> Add the GSAP library to every non-admin page
      Path list -> Add the GSAP library only to specific paths
      Content types -> Add the GSAP library only to specific content types
      Plugins -> Select the GSAP plugins to enable on your website
    3. Navigate to Administration > Appearance > Settings > [the enabled theme] and configure the animations for each paragraph types and its paragraphs fields.

## MAINTAINERS

Current maintainers:

- [Henrique Mendes (hmendes)](https://www.drupal.org/u/hmendes)
- [Rafael Oliveira (rafaolf)](https://www.drupal.org/u/rafaolf)
